module TopsailStarter
  class Engine < ::Rails::Engine
    # we are not isolating the namespace so that partials here can access routes from the "wrapping" app
    # isolate_namespace TopsailStarter

    # list all gems for which we want to pull in assets to use in the calling app here!
    require 'rails-assets-select2'
  end
end

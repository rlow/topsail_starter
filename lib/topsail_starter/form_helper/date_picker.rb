module TopsailStarter
  module FormHelper
    module DatePicker

      def date_picker(object_name, method, options = {})
        raise ":date_picker is depricated; use :date_field instead"
      end

      def datetime_picker(object_name, method, options = {})
        raise ":datetime_picker is depricated; use :datetime_local_field instead"
      end

    end
  end
end

ActionView::Base.send :include, TopsailStarter::FormHelper::DatePicker
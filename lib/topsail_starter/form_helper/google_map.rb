module TopsailStarter
  module FormHelper
    module GoogleMap
      def google_map(selector_string_for_geo_lookup_fields, html_opts={})
        concat javascript_include_tag("https://maps.google.com/maps/api/js?sensor=false")
        concat content_tag(:div, '', html_opts)
        javascript_tag("
            var $map = TopsailStarter.elementBeforeThisScriptNode();
            TopsailStarter.GoogleMap.initialize($map, $('#{selector_string_for_geo_lookup_fields}'));",
            class: 'needs_processing')
      end
    end
  end
end

ActionView::Base.send :include, TopsailStarter::FormHelper::GoogleMap
#
# Note: this uses the #to_js_hash helper!
#
module TopsailStarter
  module FormHelper
    module Select2

      def autocomplete(object_name, method, choices, options = {}, dummy = {})
        # options includes any html_options - mashed into one - see form builder for notes about fixing up
        text_field(object_name, method, options) +
        javascript_tag( "TopsailStarter.inputBeforeThisScriptNode().select2({
                            createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                              multiple: false,
                              data: #{ choices.map{|c| {id:c, text:c}}.to_json.html_safe }
                            })",
                         class: 'needs_processing')
      end

      def select2(object_name, method, choices, options = {}, html_options = {})

        logger.warn("DEPRICATED select2 helper. Use standard select helper with is='ts-select2' attribute instead!")

        html_options[:is] = 'ts-select2'

        # backward compatibility
        html_options[:style] = "width: #{options[:width]}" if options[:width].present?
        options[:prompt] = options[:placeholder] if options[:placeholder].present?

        select(object_name, method, choices, options, html_options)
      end

      def tag_select(object_name, method, choices, options = {}, html_options = {})
        raise "Use <SELECT multiple data-tags=true> instead!"
      end

    end
  end
end

ActionView::Base.send :include, TopsailStarter::FormHelper::Select2
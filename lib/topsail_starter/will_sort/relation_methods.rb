module TopsailStarter
  module WillSort
    module RelationMethods

      def _sorted_by_values(sort_strings)
        sorted_by = ActiveSupport::OrderedHash[sort_strings.map{|s|
          parts = s.split
          [parts[0].to_sym, parts[1] && parts[1].downcase == 'desc' ? :desc : :asc]
        }] # {sort_key_1: :asc, sort_key_2: :desc, ...}

        answer_rel = self
        vals_to_go = sorted_by.size

        sorted_by.each{ |sorted_by_value, sort_dir|
          sort_recipe = sort_recipies[sorted_by_value] || "#{self.table_name}.#{sorted_by_value}"
          vals_to_go -= 1

          if sort_recipe.is_a?(Proc)
            ordering_scope = sort_recipe.call
            if sort_recipe.is_a?(Proc) && vals_to_go > 0 # not last sort key
              # we need to drop possible defined secondary sorts
              ordering_scope.order_values = ordering_scope.order_values[0..0] if ordering_scope.order_values.size > 1
            end
          else
            ordering_scope = order(sort_recipe)
          end

          if sort_dir == :asc
            answer_rel = answer_rel.merge(ordering_scope)
          else
            reverse_order_values = ordering_scope.order_values.map{|o|
              # (at least) Rails 4.x is not able to intelligently reverse order when an
              # ORDER BY contains functions and commas. Our approach is to give the programmer the ability
              # to specify where ASC and DESC will be inserted. If the default implementation (append ' DESC' to
              # each comma separated part of the string) doesn't work for you, you can specify ASC and DESC wherever
              # it needs to be, but then you have to specify it wherever you might need it in that ORDER BY string!
              order_by_string = o.to_s
              if order_by_string.downcase.include?(' asc') || order_by_string.downcase.include?(' desc')
                order_by_string.gsub(/\sasc/i, ' asc').gsub(/\sdesc/i, ' desc').  # normalize
                                gsub(/\sasc/,  ' DESC').gsub(/\sdesc/, ' ASC')
              else
                order_by_string.to_s.split(',').map! do |s|
                  s.strip.concat(' DESC')
                end
              end
            }
            ordering_scope.order_values = reverse_order_values
            answer_rel = answer_rel.merge(ordering_scope)
          end

        }
        # somhow the extension gets lost, need to re-extend here
        answer_rel = answer_rel.extending(RelationMethods)
        answer_rel.instance_variable_set('@sorted_by', sorted_by)

        answer_rel
      end

      # {sort_key_1: :asc, sort_key_2: :desc, ...}
      def sorted_by_hash
        @sorted_by ? @sorted_by.clone : {}
      end

      def sort_index(sort_key)
        @sorted_by && @sorted_by.keys.index(sort_key.to_sym)
      end

      def sorted_by?(sort_key)
        @sorted_by && sort_key && @sorted_by.keys.include?(sort_key.to_sym)
      end

      # (nil, :asc, :desc)
      def sort_dir(sort_key = nil)
        if @sorted_by.nil?
          nil
        elsif sort_key.nil? && @sorted_by.size == 1
          @sorted_by.values.first
        elsif sort_key.nil?
          nil
        else
          @sorted_by[sort_key.to_sym]
        end
      end



      #
      #
      #
      def primary_sort_attr
        primary_order_attr = order_values.first
        primary_order_attr_parts = primary_order_attr.split(' ')
        primary_order_attr = primary_order_attr_parts[0..-2].join(' ') if ['asc','desc'].include?(primary_order_attr_parts.last.downcase)
        primary_order_attr
      end

      def primary_sort_type
        result_value = limit(1).reorder(nil).pluck(primary_sort_attr).first
        case result_value
          when NilClass; :nil
          when String; :string
          when Date; :date
          when Time; :time
          when Numeric; :numeric
        end
      end

      def rank_for_value(value)
        primary_sort_dir = sorted_by_hash.values.first
        where("#{primary_sort_attr} #{primary_sort_dir == :asc ? '<' : '>'} ?", value.downcase).count + 1
      end

    end
  end
end
# ActiveRecord classes can call #will_sort(mapper), where mapper defines recipies how to sort relations.
#
# Afterwards, you can ask the AR/Relation for #sorted_by('sort_key'), #sorted_by('sort_key desc'),
#             #sorted_by(['sortkey1', 'sortkey2', ...])
# The resulting relation will have an appropriate #order clause,
# and also answers to the new methods #sorted_by, #sorted_by?(sort_key), #sort_index, #sort_dir(sort_key),
#             #sortable_by?(sort_key) and #sortable_by
#
#
# mapper is a hash that maps a sort_key to a
#  a) string that can be used as an ORDER BY clause
#  b) a lambda scope (e.g.   ->{includes('owner').order('owner.name')}
#  The key :default has special meaning and will be used if no explicit sort_key was given.
#     The value of :default should be either another sort_key or string for the ORDER clause
# If a sort_key is not found in the mapper, it is passed through as the ORDER string as a default.

require 'topsail_starter/will_sort/relation_methods'

module TopsailStarter
	module WillSort
		module ActiveRecordExtension
			extend ActiveSupport::Concern

			included do
			end

			module ClassMethods
			    def will_sort(new_mapper = nil)
			      @will_sort = new_mapper.symbolize_keys
			    end

			    def sorted_by(what = nil)

						rel = all.extending(TopsailStarter::WillSort::RelationMethods) # get all the goodness from the rel_methods

						what_s_ary = Array(what).map(&:to_s) # create an array of sort strings; if waht == nil the array will be empty
						recipies = sort_recipies
						what_s_ary << recipies[:default].to_s if what_s_ary.empty? && recipies[:default].present?

						rel = rel._sorted_by_values(what_s_ary) unless what_s_ary.empty? # all the work happens here!

			      rel
			    end

			    def sortable_by
			    	sort_recipies.keys - [:default]
			    end

			    def sortable_by?(sort_key)
			    	sortable_by.include?(sort_key.to_sym)
			    end

			    # will be shadowed by the implementation in the relation later on
			    def sorted_by?(sort_key)
			    	false
			    end

					# will be shadowed by the implementation in the relation later on
			    def sorted_by_hash
			    	{}
			    end

			    # will be shadowed by the implementation in the relation later on (nil, :asc, :desc)
			    def sort_dir(sort_key)
			    	nil
			    end

			    # will be shadowed by the implementation in the relation later on
			    def sort_index(sort_key)
			    	nil
			    end

			    # Dive through the ancestors and find an over-all hash of sort recipies
					def sort_recipies
						answer = {}
						ancestors.reverse.each{ | klass |
							sorts_for_that_class = klass.instance_variable_get('@will_sort')
							answer = answer.merge(sorts_for_that_class) unless sorts_for_that_class.blank?
						}
						answer
					end
			end
		end
	end

end

ActiveRecord::Base.send :include, TopsailStarter::WillSort::ActiveRecordExtension
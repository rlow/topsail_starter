module TopsailStarter
  module WillFilter
    module RelationMethods

      # apply a filter. If we already did filter before, merge the remembered filters.
      def _filter_by(new_filters_hash)

        rel = self

        # reject filters out right if the values are not really present for that filter
        new_applicable_filters_hash = new_filters_hash.reject{|k,v|
          v.nil? ||
            v == '' ||
            ( v.is_a?(Array) && v.size == 1 && v[0].blank? )
        }.symbolize_keys

        # also reject filters if they return nil (gives filter procs the ability to reject values upon closer inspection)
        new_applicable_filters_hash.reject!{|k,v|
          filter_recipe = filter_recipies[k]
          filter_recipe.is_a?(Proc) && rel.instance_exec(v, &filter_recipe).nil?
        }

        #   when enabled, the folowing code breaks Farrins intakes filter when a custom filter is set and another filter (e.g. :specialty_names) conflicts....
        # conflicting_filter_key = new_applicable_filters_hash.keys & self.filter_keys
        # raise "This Relation was already filtered by #{conflicting_filter_key}" unless conflicting_filter_key.empty?

        @filters_hash = (@filters_hash || {}).merge(new_applicable_filters_hash)

        new_applicable_filters_hash.each{ |k,v|
          filter_recipe = filter_recipies[k]
          if filter_recipe.is_a?(Proc)
            rel = rel.instance_exec(v, &filter_recipe)
          elsif filter_recipe.nil? # plain column name
            rel = rel.where(k => v)
          else
            rel = rel.where(filter_recipe => v) # some column name
          end
        }
        # somhow the extension gets lost, need to re-extend here
        rel = rel.extending(RelationMethods)
        rel.instance_variable_set('@filters_hash', @filters_hash)
        rel
      end

      def filter_keys
        @filters_hash ? @filters_hash.keys : []
      end

      def filter_hash
        @filters_hash ? @filters_hash.clone : {}
      end

      # get a nice object for the filter that can be used in form_for;
      # E.g. you can ask it .filter.start_date_gt
      def filter
        TopsailStarter::WillFilter::Filter.new(@filters_hash)
      end

    end
  end
end
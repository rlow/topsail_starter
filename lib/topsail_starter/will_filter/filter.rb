require 'ostruct'

module TopsailStarter
  module WillFilter
    class Filter < OpenStruct
        
      include ActiveModel::AttributeMethods
      extend ActiveModel::Naming
      
      def [](key)
        self.send key
      end

    end
  end
end
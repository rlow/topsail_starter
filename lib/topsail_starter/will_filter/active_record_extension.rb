require 'topsail_starter/will_filter/relation_methods'
require 'topsail_starter/will_filter/filter'

module TopsailStarter
	module WillFilter
		module ActiveRecordExtension
			extend ActiveSupport::Concern

			included do
			end

			module ClassMethods
					# will be shadowed by the implementation in the relation later on
			    def filter_hash
			    	{}
			    end

			    def will_filter(new_mapper = nil)
			        @will_filter = new_mapper.symbolize_keys
			    end

			    def filter_by(filters_hash = nil)
							all.extending(TopsailStarter::WillFilter::RelationMethods).
			      		  _filter_by(filters_hash || {})
			    end

			    def filterable_by?(filter_key)
			    	filter_recipies.include?(filter_key.to_sym)
			    end

			    def filterable_by
			    	filter_recipies.keys
			    end

					# Dive through the ancestors and find an over-all hash of filter recipies
					def filter_recipies
						answer = {}
						ancestors.reverse.each{ | klass |
							filter_for_that_class = klass.instance_variable_get('@will_filter')
							answer = answer.merge(filter_for_that_class) unless filter_for_that_class.blank?
						}
						answer
					end
			end

		end
	end

end

ActiveRecord::Base.send :include, TopsailStarter::WillFilter::ActiveRecordExtension
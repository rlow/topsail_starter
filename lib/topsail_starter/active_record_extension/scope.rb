module TopsailStarter
  module ActiveRecordExtension
    module Scope
      extend ActiveSupport::Concern

      included do
      end

      module ClassMethods

        # creates two scopes (example uses boolean_column 'active'):
        #    active(exeptions) - returns only rows where boolean_column 'active' is true. When exception is provided
        #                        (either as a record ID, or an ActiveRecord, or an array of record IDs),
        #                        also include those rows, even if 'active' is false
        #    not_active - returns all rows for which boolean_column is false
        #
        # suggestion: boolean_column should not be nullable and have some default in the DML
        #
        def scopes_for_boolean(boolean_column)

          boolean_column = boolean_column.to_s

          scope boolean_column, lambda { | *exceptions | # nil, record id, array of record ids, an ActiveRecord or array of ActiveRecords
            where(["#{table_name}.#{boolean_column} = ? OR #{table_name}.id IN (?)",
                   true,
                   Array(exceptions[0]) ])
          }

          scope "not_#{boolean_column}", lambda {
            where boolean_column => false
          }

        end

        #
        # Creates three named scopes for the given date column:  on_or_after, on_or_before, and during
        #
        def scopes_for_date(date_column)

          date_column = date_column.to_s

          scope "#{date_column}_on_or_after", lambda { |date_from| # Date or Time
            where [ "#{table_name}.#{date_column} >= ?", date_from ]
          }

          scope "#{date_column}_on_or_before", lambda { |date_to| # Date or Time
            if date_to.is_a? Date
              where [ "#{table_name}.#{date_column} < ?", date_to.next ]
            else
              where [ "#{table_name}.#{date_column} <= ?", date_to ]
            end
          }

          scope "#{date_column}_during", lambda { |*args| # nil, Date, Date Range or Time Range
            if args[0].nil?
              range = Date.today..Date.tomorrow
            elsif args[0].is_a? Date
              range = args[0]..args[0].next
            elsif args[0].is_a? Range and args[0].end.is_a? Date
              range = args[0].begin..args[0].end.next
            else
              range = args[0]
            end
            where date_column => range
          }

        end

      end

    end
  end
end

ActiveRecord::Base.send :include, TopsailStarter::ActiveRecordExtension::Scope
module TopsailStarter
  module FormBuilder
    module DatePicker

      def date_picker(method, options = {})
        @template.date_picker(@object_name, method, objectify_options(options))
      end

      def datetime_picker(method, options = {})
        @template.datetime_picker(@object_name, method, objectify_options(options))
      end

    end
  end
end

ActionView::Helpers::FormBuilder.send :include, TopsailStarter::FormBuilder::DatePicker

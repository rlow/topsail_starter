module TopsailStarter
  module FormBuilder
    module Select2

        # we are not using html_options yet/here; need to do more research how you are supposed to pass them in
        def autocomplete(method, choices, options = {}, html_options = {})
          @template.autocomplete(@object_name, method, choices, objectify_options(options), @default_options.merge(html_options))
        end

        def select2(method, choices, options = {}, html_options = {})
          @template.select2(@object_name, method, choices, objectify_options(options), @default_options.merge(html_options))
        end

        def tag_select(method, choices = [], options = {}, html_options = {})
          current_value = Array( @object.send(method) || [] )   # ensure it is an array
          html_options[:value] ||= current_value.join(',')
          @template.tag_select(@object_name, method, choices + current_value, options, html_options)
        end

    end
  end
end


ActionView::Helpers::FormBuilder.send :include, TopsailStarter::FormBuilder::Select2
<% module_namespacing do -%>
class <%= class_name %>IndexTable < ResourcesDataTableAjaxy

  def column_definitions
    {
        id:    ->(r){ r.id },
        '' =>  ->(r){ link_to('Edit', polymorphic_url(r, action: 'edit'), remote: true, class: 'edit') if can?(:edit,r) }
    }
  end

end
<% end -%>
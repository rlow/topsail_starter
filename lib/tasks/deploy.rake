namespace :topsail_starter do

  DEFAULT_EXCLUDES = ['/log', '/tmp', '/doc', '/db', '/test', '/.gem', '/sqlnet.log', '.DS_Store',
              '/.project', '/.idea',
              '.git*', '.gitattributes', '.gitignore',
              '/.bundle', '/vendor/bundle', # bundler settings should be local to the deployment env. Making sure to not copy DEV's over
              '/public/assets',
              '/public/images/calendar_date_select', '/public/javascripts/calendar_date_select', '/public/stylesheets/calendar_date_select',
              '/public/system/dragonfly']

  task :deploy, [:ssh_connect, :app_base_path, :env, :asset_clean, :ssh_shell_init_cmd] do |t, args|

    validate_deploy_ignore()

    args.with_defaults(env: 'production', asset_clean: false, ssh_shell_init_cmd: nil)
    s_con = args[:ssh_connect]
    path = args[:app_base_path]
    ssh_shell_init_cmd = args[:ssh_shell_init_cmd] && (args[:ssh_shell_init_cmd] + ';')

    # sync files
    sh "rsync -rCv '#{Rails.root}'/ --delete --exclude-from='#{Rails.root}/.deploy_ignore' #{s_con}:#{path}"

    # bundle
    sh "ssh #{s_con} \"#{ssh_shell_init_cmd} cd #{path}; bundle install --deployment --without test development doc\""

    # changes in some gem assets (css only?) are often not detected, and we must help the asset compilation task
    # sh "ssh #{s_con} \"#{ssh_shell_init_cmd} rm -rf #{path}/tmp/cache/assets\""

    # create assets
    sh "ssh #{s_con} \"#{ssh_shell_init_cmd} cd #{path}; bundle exec rake #{'assets:clean' if args[:asset_clean]} assets:precompile RAILS_ENV=#{args[:env]}\""

    # restart
    sh "ssh #{s_con} \"#{ssh_shell_init_cmd} mkdir -p #{path}/tmp; echo `date` > #{path}/tmp/restart.txt\""
  end


  # writes version stats in public/version.html
  task :log_deployment, [:ssh_connect, :app_base_path] do |t, args|

    verfile = "#{Rails.root}/public/version.html"

    system("echo '<html><body>' > #{verfile}; git describe --abbrev=0 --tags >> #{verfile}")
    system("echo '<br>Deployed #{Time.now}' >> #{verfile}")
    system("echo '<br>' >> #{verfile}; git log -n 1 | grep commit >> #{verfile}")
    system("echo '</body></html>' >> #{verfile}")

    sh "scp #{verfile} #{args[:ssh_connect]}:#{args[:app_base_path]}/public/version.html"

    File.delete(verfile) if File.exists?(verfile)

  end

  private
  def validate_deploy_ignore
    unless File.file?("#{Rails.root}/.deploy_ignore")
      puts "Your aplication's root directory must have a .deploy_ignore file!"
      puts "A good file to start with could be:"
      puts
      DEFAULT_EXCLUDES.each{ |f| puts f }
      puts
      raise "File /.deploy_ignore missing!"
    end
  end

end

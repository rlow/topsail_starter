# desc "Explaining what the task does"
# task :topsail_starter do
#   # Task goes here
# end
namespace :topsail do

  class MarkdownHtmlRenderer < Redcarpet::Render::HTML
     def block_code(code, language)
      if language == 'code_doc'
        model = code.split[0]
        tag = code.split[1]
        "I'd now load the #{tag} from model file #{model}"
      else
        html_escaped = code.gsub(/['&\"<>\/]/, {'&' => '&amp;','<' => '&lt;','>' => '&gt;','"' => '&quot;',"'" => '&#x27;',"/" => '&#x2F;'})
        "<pre>" \
          "<code>#{html_escaped}</code>" \
        "</pre>"
      end
     end
   end

   desc "Rummage through markdown in /doc/help and generate html documents, replacing placeholders with code comments"
   def generate_docs
      markdown = Redcarpet::Markdown.new(MarkdownHtmlRenderer.new, fenced_code_blocks: true)

      Dir["#{Rails.root}/doc/help/*.md"].each{ | filename |
        outfile_name = "#{ Rails.root }/doc/help/html_out/#{ File.basename(filename).gsub(/\.md$/, '.html') }"
        raw_content = File.read(filename)
        File.open(outfile_name, 'w') { |file|
          file.write( markdown.render(raw_content).html_safe )
        }
      }
   end

end
# until pull request #172 is in will_paginate, we need to monkey patch it to take a :scope aparameter for mountable engine routes
# https://github.com/mislav/will_paginate/pull/172
module WillPaginate
  module ActionView
    protected
    class LinkRenderer < ViewHelpers::LinkRenderer
      protected
      def url(page)
        @base_url_params ||= begin
          url_params = merge_get_params(default_url_params)
          url_params[:only_path] = true
          merge_optional_params(url_params)
        end

        url_params = @base_url_params.dup
        add_current_page_param(url_params, page)

        #
        # The following changed in this patch
        #
        if (routing_scope = url_params.delete(:scope))
          return routing_scope.url_for(url_params)
        else
          return @template.url_for(url_params)
        end
      end
    end
  end
end
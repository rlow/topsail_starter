# the TopsailStarter engine assumes that the route is mounted with name 'topsail_starter'.
#
#   Usage in application's routes.rb:
#               mount TopsailStarter::Engine => "/shared", as: 'topsail_starter'

TopsailStarter::Engine.routes.draw do

  # Audit Logs
  resources 'audit_logs', controller: 'topsail_starter/audit_logs', only: ['index', 'show'] do
    get 'show_history', on: :collection
  end

end

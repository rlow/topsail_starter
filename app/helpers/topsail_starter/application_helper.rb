module TopsailStarter
  module ApplicationHelper

    #
    # Deal with JS object
    #
    #  Wrapping a string in a js_literal with allow you to have javascript code as a hash value, when you serialize the hash via #to_js_hash.
    #
    # Example:    to_js_hash({
    #                           width: 13,
    #                           onclick: js_literal("function(){alert('Hi there')}")
    #             })
    #
    def js_literal(text)
      JavascriptLiteral.new(text)
    end

    def to_js_hash(ruby_hash)
      (
        "{" +
          ruby_hash.map{|k,v|
            "\"#{ k }\": " + (v.is_a?(Hash) ? to_js_hash(v) : v.to_json)
          }.join(", ").html_safe +
        "}"
      ).html_safe
    end

    private
    class JavascriptLiteral

      def initialize(text)
        @text = text
      end

      def to_json
        @text
      end

    end

  end
end

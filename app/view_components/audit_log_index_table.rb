class AuditLogIndexTable < ResourcesDataTableAjaxy

  def column_definitions
    current_filter = records.filter.to_h
    filtered_by_one_audited = records.filter_keys.include?(:audited_type) && records.filter_keys.include?(:audited_id)

    {
      created_at: ->(l){
        link_to l(l.created_at), topsail_starter_router.audit_log_path(l)
      },
      audited:    ->(l){
        safe_join [
          link_to_unless(filtered_by_one_audited,
                         "#{l.audited_type} ##{l.audited_id}",
                         topsail_starter_router.audit_logs_path(
                            {filter: {audited_type: l.audited_type, audited_id: l.audited_id}}
                         )),
          tag(:br),
          # Note: we could reconstitute a deleted record like Address.new(Hash.from_xml(AuditLog.last.attributes_xml)['address'])
          l.audited_inspect
        ]},
      created_by: ->(l){
         link_to_unless(current_filter[:created_by_id].present?,
                        l.created_by.display_name,
                        topsail_starter_router.audit_logs_path( {filter: {created_by_id: l.created_by_id}.merge(current_filter)} )) if l.created_by
      },
      changes:    ->(l){
        safe_join [
          content_tag(:b, l.action.titleize + (l.is_a?(AssociationAuditLog) ? "Association #{l.association_name}" : '')),
          ":",
          l.is_a?(AssociationAuditLog) ?
            "#{l.associated_type} # #{l.associated_id}" : (
              l.action == AuditLog::DELETE_ACTION ?
                  'DELETE' :
                  safe_join((l.record_changes || {}).map{ |key, values|
                    content_tag :div, style:"padding-left:20px" do
                      content_tag(:b, key) + h(": #{values[0]} => #{values[1]}")
                    end
                  })
            )
        ]
      }
    }
  end

  def router
    root_context.topsail_starter_router
  end

end

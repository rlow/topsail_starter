#
# This is just a default fallback in case you didn't define a more specific DataTable component for your resource
#
class GenericDataTable < DataTableAjaxy

  attr_accessor :column_definitions

end

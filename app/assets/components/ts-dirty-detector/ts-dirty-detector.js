document.registerElement('ts-dirty-detector', {
  prototype: Object.create(
    HTMLElement.prototype,
    {

      //
      //   Attributes
      //
      disabled:      WC.boolean_attribute_accessors("disabled"),
      dirty_message: WC.attribute_accessors("dirty-message", "You will lose all unsaved changes."),

      reset: {value: function(){ this._memorize() }},

      dirty: {
        get: function(){
          var answer = false,
              current_nodelist = this._currentNodeList;
          if (current_nodelist.length != this.memorizedNodeList.length){
            answer = true;
          } else {
            for(var j = current_nodelist.length; j > 0; j--){
              var memorized_item = this.memorizedNodeList[j - 1],
                  current_node = current_nodelist[j - 1];
              if(memorized_item.name != current_node.name || memorized_item.value != current_node.value){
                answer = true;
                break;
              }
            }
          }
          return answer;
        }
      },

      //
      //    Private
      //

      // a live nodelist
      _currentNodeList: {get: function(){ return this.form.querySelectorAll("input, select, textarea") }},

      createdCallback: {
        value: function(){
          this.form = WC.closest(this, "form");
          this.form.dirty_detector = this; // so that we can find a form's dirty-detector quickly

          this._memorize();

          // seems like that the _window_global_unload_handler will only get registered once (good!)
          window.addEventListener('beforeunload', this._window_global_unload_handler)
        }
      },

      _memorize: {
        value: function(){
          this.memorizedNodeList = [].map.call(this._currentNodeList, function(i){ return {name: i.name, value: i.value} })
        }
      },

      _window_global_unload_handler: {
        value: function(e){
          var msg = '';
          msg = [].reduce.call(document.querySelectorAll("ts-dirty-detector"), function(msg, detector){
            return msg || (!detector.disabled && detector.dirty && detector.dirty_message)
          }, msg);

          if (msg){
            e.returnValue = msg;
          }
          return msg;
        }
      }
    }
  )
});

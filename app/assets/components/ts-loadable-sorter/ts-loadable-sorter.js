document.registerElement('ts-loadable-sorter', {
  prototype: Object.create(
    HTMLElement.prototype,
    {
      attachedCallback: {
        value: function(){
          this.addEventListener("click", this.toggle_sort.bind(this));
          this.addEventListener("contextmenu", this.toggle_sort.bind(this));
        }
      },

      toggle_sort: {
        value: function(event){
          var is_secondary_sort = (event.type == "contextmenu"),
              sort_val = this.getAttribute("name");

          event.preventDefault();

          if (this.hasAttribute("sort-pos") && !this.hasAttribute("desc")) sort_val = sort_val + " desc";

          var l = WC.closest(this, 'ts-loadable');
          if (is_secondary_sort){
            // wipe out trailing secondary sorts
            (l.extra_params.sort || []).splice((this.getAttribute("sort-pos") || 1000) - 1);
            // append the new sort
            l.update_extra_params('sort[]', sort_val);
          } else {
            l.update_extra_params('sort[]', [sort_val]);
          }
          l.load();
        }
      }

    }
  )
})
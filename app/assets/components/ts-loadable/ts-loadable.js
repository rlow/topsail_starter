
/* This uses JQuery! */


document.registerElement('ts-loadable', {
  prototype: Object.create(
    HTMLElement.prototype,
    {
      extra_params: {
        get: function() { return this._extra_params; },
        set: function(string_or_object){ // url_reuqest params or hash/object
          this._extra_params = typeof string_or_object == "string" ?
                                          WC.serialize_as_object(string_or_object) :
                                          string_or_object;
        }
      },
      update_extra_params: {
        value: function(param_name, value){
          if (!(typeof this._extra_params == "object")) this._extra_params = {};
          WC.railsy_update_property_hash(this._extra_params, param_name, value);
        }
      },
      extra_params_form: {
        set: function(form_el){
          form_el.addEventListener("submit",function(){
              this.extra_params = WC.serialize_as_string(form_el);
              this.load();
          }.bind(this));
        }
      },
      load: {
        value: function(){
          var el = this;

          el.classList.add("loading");

          WC.$get(this.getAttribute('href'), this.extra_params, function(responseText, textStatus, jqXHR){

            if (el.getAttribute("insert") == "top") {
              $(el).prepend(responseText);
            } else if (el.getAttribute("insert") == "bottom") {
              $(el).append(responseText);
            } else {
              $(el).html(responseText); // use jquery instead of innerHTML so that JS gets executed
            }

            // After updating the element's content, store some additional data
            var finder = /^X-TS-(.*?):[ \t]*([^\r\n]*)\r?$/mgi, // IE leaves an \r character at EOL
                ts_response_headers = {},
                match;
            while ( (match = finder.exec( jqXHR.getAllResponseHeaders() )) ) {
              ts_response_headers[ match[1].toLowerCase() ] = match[ 2 ];
            }
            el.ts_response_headers = ts_response_headers;

            WC.fireEvent(el, "ts-loaded");
            el.classList.remove("loading");
          });
        }
      },
      attachedCallback: {
        value: function(){

          // set initial extra_params
          this.extra_params = this.hasAttribute("extra-params") ?
                                  JSON.parse(this.getAttribute("extra-params")) :
                                  {};
          if (this.hasAttribute("extra-params-form-selector")){
            var f = document.querySelector(this.getAttribute("extra-params-form-selector"));
            if (f){
              this.extra_params_form = f;
            }
          }

          // load on startup unless told otherwise
          if (!this.hasAttribute("lazyload")){ this.load(); }
        }
      }
    }
  )
});

/* This uses JQuery! */


document.registerElement('ts-floatlabel', {
  extends: 'input',
  prototype: Object.create(
    HTMLInputElement.prototype,
    {
      createdCallback: {
        value: function(){

          WC.distribute_template(this, "#ts-floatlabel");

          this.root = this.parentNode;
          this._init_label_el();

          this.addEventListener('keyup',  this._check_value.bind(this));
          this.addEventListener('blur',   this._check_value.bind(this));
          this.addEventListener('change', this._check_value.bind(this));

          this._check_value();
        }
      },

      _check_value: {
        value: function(e){
          if( e ) {
                var keyCode         = e.keyCode || e.which;
                if( keyCode === 9 ) { return; }
          }
          var thisElement  = $(this),
              currentFlout = thisElement.data('flout');
          if( thisElement.val() !== "" ) { thisElement.data('flout', '1'); }
          if( thisElement.val() === "" ) { thisElement.data('flout', '0'); }
          if( thisElement.data('flout') === '1' && currentFlout !== '1' ) {
              this.show_label();
          }
          if( thisElement.data('flout') === '0' && currentFlout !== '0' ) {
              this.hide_label();
          }

        }
      },

      show_label: {
        value: function() {
          this.root.classList.add("ts-floatlabel-active")
        }
      },
      hide_label: {
        value: function() {
           this.root.classList.remove("ts-floatlabel-active")
        }
      },

      _init_label_el: {
        value: function(){
          this.label = this.root.querySelector("label");
          this.label.innerHTML = this.getAttribute("placeholder");
        }
      }


    }
  )
});
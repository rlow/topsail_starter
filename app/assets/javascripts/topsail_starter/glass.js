var TopsailStarter = TopsailStarter || {};

TopsailStarter.Glass = {

  start: function(not_glassed_el){
    $(not_glassed_el)
      .before("<div class='glass'></div>")
      .addClass("no_glass");
  },

  end: function(){
    $("div.glass").remove();
    $("div.no_glass").removeClass("no_glass");
  }

}
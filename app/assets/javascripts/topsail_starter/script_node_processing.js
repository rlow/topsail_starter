var TopsailStarter = TopsailStarter || {};

/* finds the first <script> element in the document with class "needs_processing",
   removes that class and returns that script node
*/
TopsailStarter.processingScriptNode = function(){
  var $currentScript = $("script[class=needs_processing]:first");
  $currentScript.removeClass("needs_processing");
  return $currentScript;
};

TopsailStarter.elementBeforeThisScriptNode = function(){
  var $currentScript = TopsailStarter.processingScriptNode();
  return $currentScript.prev();
};

// Similar to elementBeforeThisScriptNode, but it might dive down to find an INPUT (accout for validation error markup)
TopsailStarter.inputBeforeThisScriptNode = function(){
  var $prior = TopsailStarter.elementBeforeThisScriptNode();

  if($prior[0].nodeName == "INPUT" || $prior[0].nodeName == "SELECT")
    return $prior
  else
    return $prior.find("input:last, select:last");
};

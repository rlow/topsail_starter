//= require ./glass.js
//= require ./effect.js
//= require ./growl.js

var TopsailStarter = TopsailStarter || {};

TopsailStarter.DataTable = {

  /*
   *  - take all a[data-remote] links in the table. Register AJAX callbacks one of the following ways
   *      a) If the link is part of a .resource_form:
   *            replace the current TR with the AJAX result
   *      b) If the link is not part of a resource form, and not an index_action:
   *            change the current TR so that it only contains a single TD with the Ajax result
   *      c) If the link is an index_action:
   *            append a row to the $table containing a single TD with the AJAX result
   *  - For a form.resource_form[data-remote] do
   *      a) on success: replace current TR with result
   *      b) on failure: change the current TR so that it only contains a single TD with the Ajax result
   */
  initialize_ajaxy: function($table){
    //
    // Links
    //
    //    Not sure if I am using this one here anymore. May be it was originally meant to be used for the "Cancel" button?
    // TopsailStarter.DataTable.ajaxy_replacing_tr( $table, "ajax:success", ".resource_form a[data-remote]" );

    TopsailStarter.DataTable.ajaxy_updating_tr(  $table, "ajax:success", "a[data-remote]:not(.resource_form a):not(tfoot a)" );
    $table.on("ajax:error",  "a[data-remote]:not(.resource_form a):not(tfoot a)", function(event, jqXHR , ajaxSettings, thrownError){
      TopsailStarter.Growl.error("Unexpected Error");
    });

    TopsailStarter.DataTable.ajaxy_appending_tr( $table, "ajax:success", "tfoot a[data-remote]" ); // e.g. "New ..." action
    $table.on("ajax:error",  "tfoot a[data-remote]", function(event, jqXHR , ajaxSettings, thrownError){
      TopsailStarter.Growl.error("Unexpected Error");
    });

    // Forms
    TopsailStarter.DataTable.ajaxy_replacing_tr( $table, "ajax:success", "form.resource_form[data-remote]" );
    TopsailStarter.DataTable.ajaxy_updating_tr( $table, "ajax:error",    "form.resource_form[data-remote]", false, false );

    // in case we have images to submit
    $table.on("ajax:remotipartSubmit", "form.resource_form[data-remote]", function(event, xhr, settings){
      settings.dataType = "html *";
    });

  },

  /* Ajax results of all elements matching link_selector replace the wrapping TR */
  ajaxy_replacing_tr: function($table, event_name,  link_selector){
    $table.on(event_name, link_selector, function(e, data, status, xhr){
      TopsailStarter.Glass.end();
      var $this = $(this),
          $html = $(typeof(data) == "string" ? data : data.responseText), // might be "", e.g. on DELETE
          $row = $this.closest("tr"),
          $collapsible = $row.find("> TD > *").last(); // last() shouldn't be necessary, but I had some issues...

      // Growl
      if (xhr.getResponseHeader("Content-Description"))
        TopsailStarter.Growl.info(xhr.getResponseHeader("Content-Description"));

      // Remotipart returns the result wrapped in an Array, escaped; resolving this here if necessary
      if ($html.attr("data-type") == "text/html")
        $html = $($html.text());

      // do the actual update
      $collapsible.slideUp("slow", function(){
        $row.replaceWith($html);
        TopsailStarter.Effect.highlight($html);
      });
      return false;
    })
  },

  /* Ajax results of all elements matching link_selector go into a single TD in the wrapping TR.
     Before replacing the content, remember the prior content in data("data_table_original_content")
   */
  ajaxy_updating_tr: function($table,
                              event_name,
                              link_selector,
                              animate, /* =true */
                              remember_old_content /* =true */ ){

    if (animate == undefined) animate = true;
    if (remember_old_content == undefined) remember_old_content = true;

    $table.on(event_name, link_selector, function(e, data, status, xhr){

      var $new_data = $(typeof(data) == "string" ? data : data.responseText).hide(),
          $updating_row = $(this).closest("tr");

      // Remember the old content
      if (remember_old_content)
        $updating_row.data("data_table_original_content", $updating_row.html());

      TopsailStarter.DataTable._empty_cell( $updating_row ).html($new_data); // Replace Content

      if (animate) { // with animation
        $new_data.slideDown('slow', function(){
          TopsailStarter.Effect.highlight(this);
          TopsailStarter.DataTable._scroll_into_view($(this));
        });
      } else {
        $new_data.show();
      }

      TopsailStarter.Glass.start($new_data); // add Glass

      return false;
    })
  },

  /* Replace the TR inner html with whatever we stored for such a case in that TR's  data("data_table_original_content").
     Just remove the TR if it has class ".remove_on_bail" (from NEW action) */
  restore_original_tr: function($tr){
    if ($tr.hasClass("remove_on_bail")) {
      $tr.fadeOut("fast", function() { $(this).remove(); });
    } else {

      $tr.find("> TD > *").last().slideUp("fast", function(){
         $tr.html($tr.data("data_table_original_content"));
         $tr.show();
      });
    }
    return false;
  },


  /* takes the AJAX result from $action_link and stuffs the resulting data in $table  */
  ajaxy_appending_tr: function($table, event_name, link_selector){
    $table.on(event_name, link_selector, function(e, data, status, xhr){
      var num_columns = TopsailStarter.DataTable._num_columns($table.find("tr:first"));
      var $new_data = $("<tr class='remove_on_bail'><td colspan=" + num_columns + ">" + data + "</td></tr>"),
          $collapsible = $new_data.find("> TD > *").hide();
      $table.find("tbody:last").append($new_data);
      $collapsible.slideDown('slow', function(){
        TopsailStarter.Effect.highlight(this);
        TopsailStarter.DataTable._scroll_into_view($(this));
      });

      TopsailStarter.Glass.start($collapsible); // add Glass
    })
  },

  _empty_cell: function($tr){
    $tr.html("<td colspan=" + TopsailStarter.DataTable._num_columns($tr) + "></td>");
    return $tr.children();
  },

  _num_columns: function($tr){
    var num_cols = 0;
    $tr.children().each(function(td){
      num_cols += this.getAttribute('colspan') || 1
    });
    return num_cols;
  },

  // if bottom of $el is lower than viewport, scroll the top of $el to the top of the viewport
  _scroll_into_view: function($el){
    if ($el.offset().top + $el.outerHeight() > $(window).height() + $(document).scrollTop()){
      $("body").animate({ scrollTop: $el.offset().top }, 1000);
    }
  }

}

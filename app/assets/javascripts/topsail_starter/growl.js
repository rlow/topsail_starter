//= require jquery.freeow

var TopsailStarter = TopsailStarter || {};

TopsailStarter.Growl = {

  _msg: function(subject, content, sticky /* = false */){
    var $growl_div = $("#growl_msg");
    if ($growl_div.size() == 0){
      $("body").append('<div id="growl_msg" class="freeow freeow-top-right"></div>');
      $growl_div = $("#growl_msg");
    }
    return $growl_div.freeow( subject, content || '&nbsp;', {classes: ['gray'], autoHide: !sticky} ).children().first();
  },

  info: function(subject, body_html){
    TopsailStarter.Growl._msg(subject, body_html).addClass("info");
  },

  warn: function(subject, body_html){
    TopsailStarter.Growl._msg(subject, body_html);
  },

  error: function(subject, error_html){
    TopsailStarter.Growl._msg(subject, error_html, true).addClass("error");
  },

  errors: function(subject, errors_ary){
    var error_html = '<ul>' +
      errors_ary.map(function(e){
        return '<li>' + e + '</li>'
      }).join("") +
      "</ul>";

    return TopsailStarter.Growl.error(subject, error_html, true);
  }
}
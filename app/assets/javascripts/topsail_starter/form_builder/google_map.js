TopsailStarter.GoogleMap = {};

TopsailStarter.GoogleMap.default_options = function(){
  return  {
      center: new google.maps.LatLng(35.225236,-82.836716),
      zoom: 6,
      panControl: false,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
  }
};

TopsailStarter.GoogleMap.initialize = function($mapDiv, $geo_lookup_fields, map_options){

  var full_map_opts = $.extend({}, TopsailStarter.GoogleMap.default_options(), map_options);
  var map_struct = {
    map: new google.maps.Map($mapDiv[0], full_map_opts),
    geocoder: new google.maps.Geocoder(),
    $geo_lookup_fields: $geo_lookup_fields,
    marker: null
  };

  $geo_lookup_fields.on("blur", function(){
    TopsailStarter.GoogleMap.geocode(map_struct);
  })
};


TopsailStarter.GoogleMap.geocode = function(map_struct){

  var field_values = map_struct.$geo_lookup_fields.map(function(){return this.value}),
      num_blank_fields = $.grep(field_values, function(a){ return !a }).length;

  if ( num_blank_fields == 0 ) { // all fileds have values
      address = $.makeArray(field_values).join(", ");
      map_struct.geocoder.geocode( {'address': address}, function(results, status) { 
        if (status == google.maps.GeocoderStatus.OK) {
          if (map_struct.marker) {
            map_struct.marker.setPosition(results[0].geometry.location);
          } else {
            map_struct.marker = new google.maps.Marker({map: map_struct.map, position: results[0].geometry.location});
          }
          map_struct.map.setCenter(results[0].geometry.location);
          map_struct.map.setZoom(15);
        } else {
          alert(status);
        }
      });
  }
};

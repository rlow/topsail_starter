module Authentication

  extend ActiveSupport::Concern

  included do
    
    before_filter :authenticate_user_profile!
    around_filter ::Audit::CurrentUserForModelsFilter.new 
    
    alias_method :current_user, :current_user_profile
    helper_method :current_user
    
  end

  module ClassMethods
  end

end